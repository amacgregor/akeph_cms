# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     AkephCms.Repo.insert!(%AkephCms.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.

alias AkephCms.{Repo, Account.Users.User, Content.Project}

Repo.insert!(%User{email: "test1@somedomain.com", password_hash: "random123"})
Repo.insert!(%User{email: "test2@somedomain.com", password_hash: "random123"})
Repo.insert!(%User{email: "test3@somedomain.com", password_hash: "random123"})

Repo.insert!(%Project{title: "Test Project 1", description: "Something goes here", status: true, user_id: 1})
