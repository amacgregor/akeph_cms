defmodule AkephCms.Repo.Migrations.CreateProjects do
  use Ecto.Migration

  def change do
    create table(:projects) do
      add :title, :string
      add :description, :string
      add :status, :boolean, default: false, null: false
      add :user_id, references(:users)

      timestamps()
    end

  end
end
