# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](Https://conventionalcommits.org) for commit guidelines.

<!-- changelog -->

## [0.2.2](https://gitlab.com/amacgregor/akeph_cms/compare/0.2.1...0.2.2) (2019-2-23)




### Bug Fixes:

* ci: issue with gitlab-ci

## [0.2.1](https://gitlab.com/amacgregor/akeph_cms/compare/0.2.0...0.2.1) (2019-2-23)




### Improvements:

* update the database variables for CI

### Bug Fixes:

* ci: fix breaking gitlab configuration

## [0.2.0](https://gitlab.com/amacgregor/akeph_cms/compare/0.1.0...0.2.0) (2019-2-23)




### Improvements:

* gitlab: basic configuration for gitlab-ci

### Features:

* add absinthe library

## [0.1.0](https://gitlab.com/amacgregor/akeph_cms/compare/0.1.0...0.1.0) (2019-2-23)




### Features:

* add excoveralls

* setup credo and formater

* initialize the repository with phoenix

### Bug Fixes:

* changelog

* issue with gitops configuration
