# AkephCMS
## GraphQL HeadlessCMS 
[![pipeline status](https://gitlab.com/amacgregor/akeph_cms/badges/master/pipeline.svg)](https://gitlab.com/amacgregor/akeph_cms/commits/master)
[![coverage report](https://gitlab.com/amacgregor/akeph_cms/badges/master/coverage.svg)](https://gitlab.com/amacgregor/akeph_cms/commits/master)


### Introduction 
AkephCMS is an open-source GraphQL headless CMS, build with the goal of being extremely flexible and yet powerful for content creators and developers alike.

### Goals

TDB: Add the project goals in here

### Key Concepts 

TDB: List all the key concepts and project assumptions 

#### Data Model 

*Collection*: Defines the content model, any fields and any relationships with other collections.

- title (text)
- description (text)
- status (boolean)
- fields (jsonb)
- timestamp

---

*Entry*: Represents an individual entry that belongs to a specific collection. Entries follow the field schema defined by the collection.

- title (text)
- status (boolean)
- data (jsonb)
- timestamp

---

### ROADMAP

[Sprint 1: Basic Prototype](https://gitlab.com/amacgregor/akeph_cms/milestones/1)

### FAQ

#### Where does the name AphekCMS comes from?
The name is derived from the greek word akephalos, on modern english is acephalous which means:

1.not having a head.
