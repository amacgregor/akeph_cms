defmodule AkephCmsWeb.Schema do
  @moduledoc false

  use Absinthe.Schema
  import_types(AkephCms.Account.Users.User)
  import_types(AkephCms.Content.Project)

  query do
    @desc "Get a list of the users in the system"
    field :account_users, list_of(:user) do
      resolve(&AkephCms.UserResolver.all/2)
    end

    @desc "Retrieve a user by their email address "
    field :account_user, :user do
      arg(:email, non_null(:string))
      resolve(&AkephCms.UserResolver.find/2)
    end
  end
end
