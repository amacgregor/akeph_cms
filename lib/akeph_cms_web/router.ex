defmodule AkephCmsWeb.Router do
  use AkephCmsWeb, :router

  forward "/api", Absinthe.Plug, schema: AkephCmsWeb.Schema

  forward "/graphiql", Absinthe.Plug.GraphiQL, schema: AkephCmsWeb.Schema
end
