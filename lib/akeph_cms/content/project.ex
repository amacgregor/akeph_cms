defmodule AkephCms.Content.Project do
  use Ecto.Schema
  use Absinthe.Schema.Notation
  alias Ecto.Schema, as: Ecto_Schema
  alias Absinthe.Schema.Notation, as: Absinthe_Schema
  import Ecto.Changeset

  object :project do
    Absinthe_Schema.field(:id, :integer)
    Absinthe_Schema.field(:title, :string)
    Absinthe_Schema.field(:description, :string)
  end

  schema "projects" do
    Ecto_Schema.field :description, :string
    Ecto_Schema.field :status, :boolean, default: false
    Ecto_Schema.field :title, :string
    belongs_to :user, AkephCms.Account.Users.User

    timestamps()
  end

  @type t :: %__MODULE__{
      id: integer,
      title: String.t(),
      description: String.t(),
      status: boolean,
      user_id: integer,
      inserted_at: DateTime.t(),
      updated_at: DateTime.t()
  }

  @doc false
  def changeset(project, attrs) do
    project
    |> cast(attrs, [:title, :description, :status])
    |> validate_required([:title, :description, :status])
  end
end
