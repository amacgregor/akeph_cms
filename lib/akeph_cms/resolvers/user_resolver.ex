defmodule AkephCms.UserResolver do
  @moduledoc false

  alias AkephCms.{Account.Users}

  def all(_args, _info) do
    {:ok, Users.list_all()}
  end

  def find(%{email: email}, _info) do
    case Users.find_user_by_email(email) do
      nil -> {:error, "User email #{email} not found!"}
      user -> {:ok, user}
    end
  end

end
