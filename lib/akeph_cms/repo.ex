defmodule AkephCms.Repo do
  use Ecto.Repo,
    otp_app: :akeph_cms,
    adapter: Ecto.Adapters.Postgres
end
