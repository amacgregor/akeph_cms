defmodule AkephCms.Account.Users.User do
  use Ecto.Schema
  use Absinthe.Schema.Notation
  alias Ecto.Schema, as: Ecto_Schema
  alias Absinthe.Schema.Notation, as: Absinthe_Schema
  alias AkephCms.Content.{Project}
  import Ecto.Changeset

  object :user do
    Absinthe_Schema.field(:id, :integer)
    Absinthe_Schema.field(:email, :string)
    Absinthe_Schema.field(:projects, list_of(:project))

    # TODO: Remove from the Graphql Schema
    Absinthe_Schema.field(:password_hash, :string)
  end

  @type t :: %__MODULE__{
          id: integer,
          email: String.t(),
          password_hash: String.t(),
          projects: [Project.t()],
          inserted_at: DateTime.t(),
          updated_at: DateTime.t()
        }

  @required_fields ~w(email password_hash)a

  schema "users" do
    Ecto_Schema.field(:email, :string)
    Ecto_Schema.field(:password_hash, :string)
    has_many :projects, AkephCms.Content.Project

    timestamps()
  end

  @doc false
  def changeset(users, attrs) do
    users
    |> cast(attrs, [:email, :password_hash])
    |> validate_required(@required_fields)
    |> unique_constraint(:email)
  end
end
