defmodule AkephCms.Account.Users do
  @module """
  Secondary Context for Users

  Purpose: Handle all CRUD operations for the user resource

  Rules:
  - Every function on this module should return either a corresponding resource, a list of corresponding resources, a changeset or a list of changesets for a corresponding resource.
  - All interactions with the the repos must be done through the secondary contexts
  """

  alias AkephCms.{Repo, Account.Users.User}

  # TODO: Implement relevant features for user management and retrieval


  def list_all() do
    Repo.all(User)
  end

  def find_user_by_email(email) do
    Repo.get_by!(User, email: email)
    |>load_associated_projects()
  end

  def load_associated_projects(user) do
    Repo.preload(user, :projects)
  end

end
