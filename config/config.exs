# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :akeph_cms,
  ecto_repos: [AkephCms.Repo]

# Configures the endpoint
config :akeph_cms, AkephCmsWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "7FeCpM6UpfCWD4Z9b5AfAQwd2OVNwtjKADPVoGGc/oME++XJp9FF5FSm2aLJyr0U",
  render_errors: [view: AkephCmsWeb.ErrorView, accepts: ~w(json)],
  pubsub: [name: AkephCms.PubSub, adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
